from flask import Flask, request, jsonify
from flask_cors import CORS
import click
import boto3
import time
import logging
from datetime import datetime, timedelta

app = Flask(__name__)
CORS(app)

logger = logging.getLogger(__name__)


def configure_logging(debug):
    log_level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(
        level=log_level,
        format="[%(asctime)s] [%(levelname)s] %(message)s",
        handlers=[logging.StreamHandler(), logging.FileHandler("download_logs.log")],
    )


def get_log_groups(client, log_group):
    if log_group:
        return [log_group]
    else:
        response = client.describe_log_groups()
        return [{"logGroupName": lg["logGroupName"]} for lg in response["logGroups"]]


def get_log_streams(client, log_group, log_stream):
    if log_stream:
        return [log_stream]
    else:
        response = client.describe_log_streams(
            logGroupName=log_group, orderBy="LastEventTime", descending=True
        )
        return [stream["logStreamName"] for stream in response["logStreams"]]


def download_log_events(
    client, log_group, log_stream, start_time_epoch, end_time_epoch
):
    response = client.get_log_events(
        logGroupName=log_group,
        logStreamName=log_stream,
        startTime=start_time_epoch,
        endTime=end_time_epoch,
    )
    return response["events"]


def write_log_events_to_file(log_events, file_path):
    with open(file_path, "a") as log_file:
        for event in log_events:
            log_file.write(event["message"] + "\n")


def download_cloudwatch_logs(
    log_group, log_stream, aws_region, start_time, end_time
):
    logger.info("Starting log download process.")
    client = boto3.client("logs", region_name=aws_region)

    # Convert start and end times to epoch format
    start_time_epoch = int(start_time.timestamp() * 1000)
    end_time_epoch = int(end_time.timestamp() * 1000)

    log_groups = get_log_groups(client, log_group)
    for group in log_groups:
        logger.info(f"Downloading logs from log group: {group['logGroupName']}")

        log_streams = get_log_streams(client, group["logGroupName"], log_stream)
        for stream in log_streams:
            logger.info(f"Downloading logs from log stream: {stream}")

            events = download_log_events(
                client, group["logGroupName"], stream, start_time_epoch, end_time_epoch
            )

            file_path = f"{group['logGroupName']}_{stream}.log"
            write_log_events_to_file(events, file_path)

            logger.info(f"Downloaded log stream: {stream}")
            time.sleep(0.5)

    logger.info("All log streams downloaded and combined successfully.")


@app.route('/regions', methods=['GET'])
def get_regions_route():
    regions = boto3.Session().get_available_regions('logs')
    return jsonify(regions)


@app.route('/get_log_groups', methods=['POST'])
def get_log_groups_route():
    data = request.get_json()
    log_group = data['log_group']
    aws_region = data['aws_region']
    client = boto3.client("logs", region_name=aws_region)
    log_groups = get_log_groups(client, log_group)
    return jsonify(log_groups)


@app.route('/get_log_streams', methods=['POST'])
def get_log_streams_route():
    data = request.get_json()
    log_group = data['log_group']
    aws_region = data['aws_region']
    client = boto3.client("logs", region_name=aws_region)
    log_streams = get_log_streams(client, log_group, None)
    return jsonify(log_streams)


@app.route('/download_logs', methods=['POST'])
def download_logs_route():
    data = request.get_json()
    log_group = data['log_group']
    log_stream = data['log_stream']
    aws_region = data['aws_region']
    start_time = datetime.strptime(data['start_time'], '%Y-%m-%dT%H:%M:%S')
    end_time = datetime.strptime(data['end_time'], '%Y-%m-%dT%H:%M:%S')
    debug = data['debug']

    configure_logging(debug)

    try:
        download_cloudwatch_logs(log_group, log_stream, aws_region, start_time, end_time)
        return jsonify({"message": "Log download completed successfully."}), 200
    except Exception as e:
        error_message = f"Error occurred while downloading logs: {str(e)}"
        logger.error(error_message)
        return jsonify({"message": error_message}), 500


if __name__ == '__main__':
    app.run()
