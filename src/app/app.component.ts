import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface LogGroup {
  logGroupName: string;
}

interface LogStream {
  logStreamName: string;
}

interface Region {
  regionName: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  logGroups: LogGroup[] = [];
  logStreams: LogStream[] = [];
  selectedRegion: Region | null = null;
  selectedLogGroup: string = '';
  selectedLogStream: string = '';
  startTime: string = '';
  endTime: string = '';
  debug: boolean = false;
  errorMessage: string = '';
  awsRegions: Region[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.fetchAwsRegions();
    this.setDefaultTimeRange();
  }

  fetchAwsRegions() {
    this.http.get<Region[]>('http://localhost:5000/regions').subscribe(
      (data) => {
        this.awsRegions = data;
      },
      (error) => {
        this.errorMessage = error.error.message;
      }
    );
  }

fetchLogGroups() {
  if (this.selectedRegion) {
    console.log(this.selectedRegion); // Log the selectedRegion object to the console for debugging
    const payload = { log_group: '', aws_region: this.selectedRegion };
    this.http.post<LogGroup[]>('http://localhost:5000/get_log_groups', payload).subscribe(
      (data) => {
        this.logGroups = data;
        console.log(data)
      },
      (error) => {
        this.errorMessage = error.error.message;
      }
    );
  }
}

  fetchLogStreams() {
    if (this.selectedLogGroup) {
      const payload = { log_group: this.selectedLogGroup };
      this.http.post<LogStream[]>('http://localhost:5000/get_log_streams', payload).subscribe(
        (data) => {
          this.logStreams = data;
        },
        (error) => {
          this.errorMessage = error.error.message;
        }
      );
    }
  }

  onRegionChange() {
    this.fetchLogGroups();
  }

  onLogGroupChange() {
    this.fetchLogStreams();
  }

  setDefaultTimeRange() {
    const now = new Date();
    const oneHourAgo = new Date(now.getTime() - 60 * 60 * 1000); // Set default start time to 1 hour ago
    this.startTime = this.formatDateTime(oneHourAgo);
    this.endTime = this.formatDateTime(now);
  }

  formatDateTime(date: Date): string {
    const year = date.getFullYear();
    const month = this.padNumber(date.getMonth() + 1);
    const day = this.padNumber(date.getDate());
    const hours = this.padNumber(date.getHours());
    const minutes = this.padNumber(date.getMinutes());
    const seconds = this.padNumber(date.getSeconds());
    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}`;
  }

  padNumber(num: number): string {
    return num.toString().padStart(2, '0');
  }

  onFormSubmit() {
    if (this.validateForm()) {
      const payload = {
        log_group: this.selectedLogGroup,
        log_stream: this.selectedLogStream,
        aws_region: this.selectedRegion?.regionName,
        start_time: this.startTime,
        end_time: this.endTime,
        debug: this.debug,
      };
      this.http.post<any>('http://localhost:5000/download_logs', payload).subscribe(
        (data) => {
          console.log(data);
        },
        (error) => {
          this.errorMessage = error.error.message;
          // Show the modal
        }
      );
    }
  }

  validateForm(): boolean {
    if (!this.selectedRegion) {
      this.errorMessage = 'Please select an AWS Region.';
      // Show the modal
      return false;
    }
    if (!this.selectedLogGroup) {
      this.errorMessage = 'Please select a Log Group.';
      // Show the modal
      return false;
    }
    if (!this.selectedLogStream) {
      this.errorMessage = 'Please select a Log Stream.';
      // Show the modal
      return false;
    }
    if (!this.startTime || !this.endTime) {
      this.errorMessage = 'Please provide a valid Start Time and End Time.';
      // Show the modal
      return false;
    }
    return true;
  }
}
